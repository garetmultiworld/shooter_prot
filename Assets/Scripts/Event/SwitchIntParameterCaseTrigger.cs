﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchIntParameterCaseTrigger : MonoBehaviour
{
    public int ParameterValue;
    public TriggerInterface Trigger;
}
