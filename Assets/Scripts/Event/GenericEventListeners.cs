﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GenericEventListeners
{
    public string EventName;
    public List<GenericEventListener> listeners=new List<GenericEventListener>();

    public void TriggerStringEvent(GameObject source,string eventParameter, float EventNumber, int EventInteger)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            if (listeners[i] != null)
            {
                listeners[i].TriggerStringEvent(source,eventParameter, EventNumber, EventInteger);
            }
        }
    }
}
