﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BroadcastStimuliTrigger : TriggerInterface
{

    public string Stimuli;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        StimuliManager.Instance.BroadcastStimuli(Stimuli,gameObject);
    }

}
