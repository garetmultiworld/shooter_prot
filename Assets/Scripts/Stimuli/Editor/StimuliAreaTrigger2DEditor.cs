﻿using UnityEditor;

[CustomEditor(typeof(StimuliAreaTrigger2D), true)]
public class StimuliAreaTrigger2DEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(StimuliAreaTrigger2D trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "StimuliAreaTrigger2D");
    }

    protected void StorePrefabConflict(StimuliAreaTrigger2D trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        StimuliAreaTrigger2D stimuliAreaTrigger = (StimuliAreaTrigger2D)target;
        SetUpPrefabConflict(stimuliAreaTrigger);
        stimuliAreaTrigger.showStimuliAreaTrigger = EditorGUILayout.Foldout(
            stimuliAreaTrigger.showStimuliAreaTrigger,
            "Stimuli"
        );
        if (stimuliAreaTrigger.showStimuliAreaTrigger)
        {
            string prevString = stimuliAreaTrigger.Stimuli;
            stimuliAreaTrigger.Stimuli = EditorGUILayout.TextField("Stimuli Name", stimuliAreaTrigger.Stimuli);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(stimuliAreaTrigger.Stimuli)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(stimuliAreaTrigger.Stimuli)) ||
                (prevString != null && !prevString.Equals(stimuliAreaTrigger.Stimuli))
            )
            {
                _hadChanges = true;
            }
            float prevFloat = stimuliAreaTrigger.Radius;
            stimuliAreaTrigger.Radius = EditorGUILayout.FloatField("Radius", stimuliAreaTrigger.Radius);
            if (prevFloat != stimuliAreaTrigger.Radius)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(stimuliAreaTrigger);
    }
}
