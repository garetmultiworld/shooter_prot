﻿using UnityEditor;

[CustomEditor(typeof(StimuliAreaTrigger), true)]
public class StimuliAreaTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(StimuliAreaTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "StimuliAreaTrigger");
    }

    protected void StorePrefabConflict(StimuliAreaTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        StimuliAreaTrigger stimuliAreaTrigger = (StimuliAreaTrigger)target;
        SetUpPrefabConflict(stimuliAreaTrigger);
        stimuliAreaTrigger.showStimuliAreaTrigger = EditorGUILayout.Foldout(
            stimuliAreaTrigger.showStimuliAreaTrigger,
            "Stimuli"
        );
        if (stimuliAreaTrigger.showStimuliAreaTrigger)
        {
            string prevString = stimuliAreaTrigger.Stimuli;
            stimuliAreaTrigger.Stimuli = EditorGUILayout.TextField("Stimuli Name", stimuliAreaTrigger.Stimuli);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(stimuliAreaTrigger.Stimuli)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(stimuliAreaTrigger.Stimuli)) ||
                (prevString != null && !prevString.Equals(stimuliAreaTrigger.Stimuli))
            )
            {
                _hadChanges = true;
            }
            float prevFloat = stimuliAreaTrigger.Radius;
            stimuliAreaTrigger.Radius = EditorGUILayout.FloatField("Radius", stimuliAreaTrigger.Radius);
            if (prevFloat != stimuliAreaTrigger.Radius)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(stimuliAreaTrigger);
    }

}
