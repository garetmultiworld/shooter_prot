﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpawnTrigger), true)]
public class SpawnTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(SpawnTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "SpawnTrigger");
    }

    protected void StorePrefabConflict(SpawnTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        SpawnTrigger spawnTrigger = (SpawnTrigger)target;
        SetUpPrefabConflict(spawnTrigger);
        spawnTrigger.showSpawnTrigger = EditorGUILayout.Foldout(
            spawnTrigger.showSpawnTrigger,
            "Spawn"
        );
        Transform previewTransform;
        GameObject preview=null;
        if (spawnTrigger.showSpawnTrigger)
        {
            spawnTrigger.showPreview = EditorUtils.Checkbox(this,"Show Preview", spawnTrigger.showPreview);
            previewTransform = spawnTrigger.gameObject.transform.Find("SpawnTriggerPreview");
            if (previewTransform != null)
            {
                preview = previewTransform.gameObject;
            }
            if (!EditorApplication.isPlaying&&spawnTrigger.showPreview&& preview == null)
            {
                preview = new GameObject("SpawnTriggerPreview");
                preview.transform.parent = spawnTrigger.gameObject.transform;
            }
            else if((EditorApplication.isPlaying ||!spawnTrigger.showPreview)&&preview != null)
            {
                DestroyImmediate(preview);
            }
            if (spawnTrigger.Prefab == null)
            {
                EditorGUILayout.HelpBox("There's no prefab assigned", MessageType.Warning);
            }
            spawnTrigger.Prefab = EditorUtils.GameObjectField(
                this,
                "Prefab",
                spawnTrigger.Prefab
            );
            if (!EditorApplication.isPlaying && spawnTrigger.showPreview && spawnTrigger.prefabChanged)
            {
                spawnTrigger.prefabChanged = false;
                foreach (Transform child in preview.transform)
                {
                    DestroyImmediate(child.gameObject);
                }
                if (spawnTrigger.Prefab != null)
                {
                    GameObject instance = Instantiate(spawnTrigger.Prefab, new Vector3(0, 0, 0), Quaternion.identity);
                    instance.transform.parent = preview.transform;
                    instance.transform.localPosition = Vector3.zero;
                }
            }
            EditorGUILayout.HelpBox("If there's no parent assigned, it will spawn at the root of the scene", MessageType.Info);
            spawnTrigger.Parent = EditorUtils.GameObjectField(
                this,
                "Parent",
                spawnTrigger.Parent
            );
            if (spawnTrigger.Bounds == null)
            {
                EditorGUILayout.HelpBox("It will spawn right where the parent is, if you want to have it spawn randomly in an area you can add a RectBounds", MessageType.Info);
                if (GUILayout.Button("Add RectBounds"))
                {
                    spawnTrigger.Bounds = spawnTrigger.gameObject.AddComponent(typeof(RectBounds)) as RectBounds;
                    _hadChanges = true;
                }
            }
            spawnTrigger.Bounds = EditorUtils.RectBoundsField(
                this,
                "Bounds",
                spawnTrigger.Bounds
            );
            if (spawnTrigger.Bounds != null)
            {
                EditorGUILayout.HelpBox("If an anchor is set, the spawner won't use the full rectangle but instead it will use the line associated with the anchor.", MessageType.Info);
                spawnTrigger.Anchor = (RectBounds.AnchorType)EditorUtils.EnumPopup(
                    this,
                    "Anchor",
                    spawnTrigger.Anchor
                );
            }
            EditorGUILayout.HelpBox("A Spawn Initializator allows you to store the spawned object in its Target Holder and then fire all triggers with the assigned label.", MessageType.Info);
            spawnTrigger.SpawnInitializator = (GameObject)EditorUtils.GameObjectField(
                this,
                "Spawn Initializator",
                spawnTrigger.SpawnInitializator
            );
            if (spawnTrigger.SpawnInitializator != null)
            {
                if (spawnTrigger.SpawnInitializator.GetComponent<TargetHolder>() == null)
                {
                    EditorGUILayout.HelpBox("The Spawn Initializator doesn't have a Target Holder", MessageType.None);
                    if (GUILayout.Button("Add Target Holder to the Spawn Initializator"))
                    {
                        TargetHolder t=spawnTrigger.SpawnInitializator.AddComponent(typeof(TargetHolder)) as TargetHolder;
                    }
                }
                spawnTrigger.SpawnInitializatorTriggerLabel = EditorUtils.TextField(
                    this,
                    "Spawn Initializator Trigger Label: ",
                    spawnTrigger.SpawnInitializatorTriggerLabel
                );
            }
        }
        StorePrefabConflict(spawnTrigger);
    }

}
