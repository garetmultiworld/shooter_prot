﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showSpawnTrigger = true;
    public bool showPreview = false;
    public bool prefabChanged = true;
#endif

    public GameObject Prefab;
    public GameObject Parent;
    public RectBounds Bounds;
    public RectBounds.AnchorType Anchor;
    public GameObject SpawnInitializator;
    public string SpawnInitializatorTriggerLabel;

    void Start()
    {
        Transform previewTransform=transform.Find("SpawnTriggerPreview");
        if (previewTransform != null)
        {
            Destroy(previewTransform.gameObject);
        }
    }

    public override void Cancel()
    {}

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Prefab == null)
        {
            return;
        }
        GameObject instance=Instantiate(Prefab, new Vector3(0, 0, 0), Quaternion.identity);
        if (Parent != null)
        {
            instance.transform.parent = Parent.transform;
        }
        if (Bounds!=null)
        {
            Vector2 topLeft = Bounds.GetTopLeft();
            Vector2 bottomRight = Bounds.GetBottomRight();
            Vector3 newPosition = new Vector3(
                Random.Range(topLeft.x, bottomRight.x),
                Random.Range(topLeft.y, bottomRight.y),
                instance.transform.position.z
            );
            switch (Anchor)
            {
                case RectBounds.AnchorType.Bottom:
                    newPosition.y = Bounds.GetBottom();
                    break;
                case RectBounds.AnchorType.Top:
                    newPosition.y = Bounds.GetTop();
                    break;
                case RectBounds.AnchorType.Left:
                    newPosition.x = Bounds.GetLeft();
                    break;
                case RectBounds.AnchorType.Right:
                    newPosition.x = Bounds.GetRight();
                    break;
            }
            instance.transform.position = newPosition;
        }
        else
        {
            instance.transform.localPosition = Vector3.zero;
        }
        if (SpawnInitializator != null)
        {
            TargetHolder targetHolder = SpawnInitializator.GetComponent<TargetHolder>();
            if (targetHolder != null)
            {
                targetHolder.Target = instance;
            }
            TriggerInterface[] Triggers;
            if (!string.IsNullOrEmpty(SpawnInitializatorTriggerLabel))
            {
                Triggers = SpawnInitializator.GetComponents<TriggerInterface>();
                foreach (TriggerInterface trigger in Triggers)
                {
                    if (SpawnInitializatorTriggerLabel.Equals(trigger.GetLabel()))
                    {
                        trigger.FloatParameter = FloatParameter;
                        trigger.Fire(this);
                    }
                }
            }
            Triggers = instance.GetComponents<TriggerInterface>();
            foreach (TriggerInterface trigger in Triggers)
            {
                if (SpawnInitializatorTriggerLabel.Equals(trigger.GetLabel()))
                {
                    trigger.FloatParameter = FloatParameter;
                    trigger.Fire(this);
                }
            }
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (Prefab != null)
        {
            desc += ", "+Prefab.name;
        }
        else
        {
            desc += ", no prefab";
        }
        if (Parent != null)
        {
            desc += ", " + Parent.name;
        }
        else
        {
            desc += ", no parent";
        }
        if (Bounds != null)
        {
            desc += ", with bounds";
            switch (Anchor)
            {
                case RectBounds.AnchorType.Bottom:
                    desc += " (bottom)";
                    break;
                case RectBounds.AnchorType.Top:
                    desc += " (top)";
                    break;
                case RectBounds.AnchorType.Left:
                    desc += " (left)";
                    break;
                case RectBounds.AnchorType.Right:
                    desc += " (right)";
                    break;
            }
        }
        if (SpawnInitializator != null)
        {
            desc += ", initializator: " + SpawnInitializator.name;
            if (!string.IsNullOrEmpty(SpawnInitializatorTriggerLabel))
            {
                desc += ", (" + SpawnInitializatorTriggerLabel + ")";
            }
        }
        return desc;
    }

}
