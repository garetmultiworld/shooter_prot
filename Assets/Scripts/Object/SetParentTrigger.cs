﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetParentTrigger : TriggerInterface
{

    public GameObject TheObject;
    public Transform NewParent;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheObject.transform.SetParent(NewParent);
    }

}
