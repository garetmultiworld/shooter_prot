﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfObjectEnabledTrigger : TriggerInterface
{

    public GameObject TheObject;
    public TriggerInterface IfActive;
    public TriggerInterface IfInactive;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheObject != null)
        {
            if (TheObject.activeSelf)
            {
                if (IfActive != null)
                {
                    IfActive.Fire(this);
                }
            }
            else
            {
                if (IfInactive != null)
                {
                    IfInactive.Fire(this);
                }
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("If Active", IfActive,0);
        callbacks[1] = new TriggerCallback("If Inactive", IfInactive,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            IfActive = trigger;
        }
        else
        {
            IfInactive = trigger;
        }
    }
#endif

}
