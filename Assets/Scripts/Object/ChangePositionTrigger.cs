﻿using System.Collections;
using UnityEngine;

public class ChangePositionTrigger: TriggerInterface
{

#if UNITY_EDITOR
    public bool showChangePositionTrigger = true;
    public bool showChangePositionTriggerAnimation = true;
    public bool testingAnimation = false;
    Vector3 originalPosition;
#endif

    public Transform NewPosition;
    public GameObject Object;
    public bool Animate;
    public float MinTime = 1;
    public float MaxTime = 2;
    public AnimationCurve AnimCurveX = new AnimationCurve();
    public AnimationCurve AnimCurveY = new AnimationCurve();
    public TriggerInterface TriggerOnArrive;
    public bool Local;
    public bool DataFromTarget;
    public bool AlsoReparent;

    private bool IsAnimating = false;
    private Vector3 StartingPos;
    private Vector3 TargetPos;
    private float TotalTime;
    private float CurrentTimer = 0;
    private GameObject TheObject;
    private float StartingXScale;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        SetTargetObject();
        TargetPos = (Local) ? NewPosition.localPosition : NewPosition.position;
        if (Animate)
        {
            StartingPos = (Local) ? TheObject.transform.localPosition : TheObject.transform.position;
            TotalTime = UnityEngine.Random.Range(MinTime, MaxTime);
            CurrentTimer = 0;
            IsAnimating = true;
            StartCoroutine(ChangePosition());
        }
        else
        {
            if (Local)
            {
                TheObject.transform.localPosition = TargetPos;
            }
            else
            {
                TheObject.transform.position = TargetPos;
            }
#if UNITY_EDITOR
            if (!testingAnimation && AlsoReparent)
            {
                TheObject.transform.parent = NewPosition;
            }
            if (testingAnimation)
            {
                testingAnimation = false;
            }
#else
            if (AlsoReparent)
            {
                TheObject.transform.parent = NewPosition;
            }
#endif
            if (TriggerOnArrive != null)
            {
                TriggerOnArrive.Fire(this);
            }
        }
    }

    void SetTargetObject()
    {
        if (DataFromTarget)
        {
            TheObject = Object.GetComponent<TargetHolder>().Target;
        }
        else
        {
            TheObject = Object;
        }
    }

#if UNITY_EDITOR
    public void TestFire()
    {
        testingAnimation = true;
        SetTargetObject();
        originalPosition = TheObject.transform.position;
        Fire();
    }

    public void ResetPosition()
    {
        TheObject.transform.position = originalPosition;
    }
#endif

    private IEnumerator ChangePosition()
    {
        bool finished=false;
        while (IsAnimating)
        {
            float t = CurrentTimer / TotalTime;
            Vector3 newPos = new Vector3(
                StartingPos.x + ((TargetPos.x - StartingPos.x) * AnimCurveX.Evaluate(t)),
                StartingPos.y + ((TargetPos.y - StartingPos.y) * AnimCurveY.Evaluate(t)),
                TheObject.transform.position.z
            );
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer >= TotalTime)
            {
                newPos.x = TargetPos.x;
                newPos.y = TargetPos.y;
                IsAnimating = false;
#if UNITY_EDITOR
                if (!testingAnimation&&TriggerOnArrive != null)
                {
                    finished = true;
                    TriggerOnArrive.Fire(this);
                }
#else
                if (TriggerOnArrive != null)
                {
                    TriggerOnArrive.Fire(this);
                }
#endif
            }
            if (Local)
            {
                TheObject.transform.localPosition = newPos;
            }
            else
            {
                TheObject.transform.position = newPos;
            }
#if UNITY_EDITOR
            if (!testingAnimation&&finished && AlsoReparent)
            {
                TheObject.transform.parent = NewPosition;
            }
            if (testingAnimation)
            {
                testingAnimation = false;
            }
#else
            if (finished&&AlsoReparent)
            {
                TheObject.transform.parent = NewPosition;
            }
#endif
            yield return null;
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("On Arrive", TriggerOnArrive,0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerOnArrive = trigger;
    }
#endif

}
