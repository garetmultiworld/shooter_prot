﻿using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(EnableNextSequenceTrigger), true)]
public class EnableNextSequenceTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(EnableNextSequenceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "EnableNextSequenceTrigger");
    }

    protected void StorePrefabConflict(EnableNextSequenceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        EnableNextSequenceTrigger triggerFireableSequence = (EnableNextSequenceTrigger)target;
        SetUpPrefabConflict(triggerFireableSequence);
        triggerFireableSequence.showEnableNextSequenceTrigger = EditorGUILayout.Foldout(
            triggerFireableSequence.showEnableNextSequenceTrigger,
            "Sequence"
        );
        if (triggerFireableSequence.showEnableNextSequenceTrigger)
        {
            triggerFireableSequence.loop = EditorUtils.Checkbox(this, "Loop", triggerFireableSequence.loop);
            triggerFireableSequence.OnBegin = EditorUtils.SelectTrigger(this, "On Begin", triggerFireableSequence.OnBegin);
            triggerFireableSequence.OnEnd = EditorUtils.SelectTrigger(this, "On End", triggerFireableSequence.OnEnd);

            triggerFireableSequence.showEnableNextSequenceTriggerObjects = EditorGUILayout.Foldout(
                triggerFireableSequence.showEnableNextSequenceTriggerObjects,
                "Sequence Objects"
            );
            if (triggerFireableSequence.showEnableNextSequenceTriggerObjects)
            {
                if (triggerFireableSequence.Objects.Length == 0)
                {
                    if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                    {
                        Array.Resize(ref triggerFireableSequence.Objects, triggerFireableSequence.Objects.Length + 1);
                        _hadChanges = true;
                    }
                }
                for (int iTrigger = 0; iTrigger < triggerFireableSequence.Objects.Length; iTrigger++)
                {
                    DrawItem(triggerFireableSequence, iTrigger);
                }
            }
        }
        StorePrefabConflict(triggerFireableSequence);
    }

    protected void DrawItem(EnableNextSequenceTrigger triggerFireableSequence, int iTrigger)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref triggerFireableSequence.Objects, iTrigger);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref triggerFireableSequence.Objects, iTrigger);
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref triggerFireableSequence.Objects, iTrigger);
            _hadChanges = true;
        }
        if (iTrigger == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref triggerFireableSequence.Objects, iTrigger);
                _hadChanges = true;
            }
        }
        if (iTrigger == (triggerFireableSequence.Objects.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref triggerFireableSequence.Objects, iTrigger);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(triggerFireableSequence, iTrigger);
        }
    }

    protected void DrawItemDetails(EnableNextSequenceTrigger triggerFireableSequence, int iTrigger)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(iTrigger.ToString());

        triggerFireableSequence.Objects[iTrigger] = EditorUtils.GameObjectField(this, triggerFireableSequence.Objects[iTrigger]);
        GUILayout.EndHorizontal();
        if (iTrigger < (triggerFireableSequence.Objects.Length - 1))
        {
            EditorUtils.DrawUILine(Color.grey);
        }
    }
}
