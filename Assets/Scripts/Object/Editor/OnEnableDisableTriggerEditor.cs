﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(OnEnableDisableTrigger))]
public class OnEnableDisableTriggerEditor : EditorBase
{

    protected void SetUpPrefabConflict(OnEnableDisableTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "OnEnableDisableTrigger");
    }

    protected void StorePrefabConflict(OnEnableDisableTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        OnEnableDisableTrigger onEnableDisableTrigger = (OnEnableDisableTrigger)target;
        SetUpPrefabConflict(onEnableDisableTrigger);
        
        onEnableDisableTrigger.OnEnableTrigger = EditorUtils.SelectTrigger(this,"On Enable", onEnableDisableTrigger.OnEnableTrigger);
        onEnableDisableTrigger.OnDisableTrigger = EditorUtils.SelectTrigger(this,"On Disable", onEnableDisableTrigger.OnDisableTrigger);
        StorePrefabConflict(onEnableDisableTrigger);
    }

}
