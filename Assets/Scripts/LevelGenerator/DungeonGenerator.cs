﻿using mw.player;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : TriggerInterface
{

    public TriggerInterface EndDungeonTrigger;
    public Dungeon dungeonData;
    public TriggerInterface OnEnterLevel;
    public TriggerInterface OnLeaveLevel;
    public bool DontRepeatSections=false;

    protected LevelInstance currentLevel;
    protected int currentAmount = 0;
    protected int numLevels;
    protected List<int> sectionsChosen=new List<int>();
    protected List<LevelSection> instantiatedSections = new List<LevelSection>();


    protected GenericEventListener _sectionProximityListener;



    void Start()
    {
        Level nextLevel;
        numLevels = Random.Range(dungeonData.MinLevelAmount, dungeonData.MaxLevelAmount+1);
        if (dungeonData.StartingLevel != null)
        {
            nextLevel = dungeonData.StartingLevel[Random.Range(0, dungeonData.StartingLevel.Count)];
        }
        else
        {
            nextLevel = PickNextLevel();
        }
        InstanceLevel(nextLevel, true);
        currentAmount++;
        if (OnEnterLevel)
        {
            OnEnterLevel.Fire(this);
        }
        _sectionProximityListener = gameObject.AddComponent(typeof(GenericEventListener)) as GenericEventListener;
        _sectionProximityListener.EventName = "SectionProximity";
        _sectionProximityListener.OnEvent = this;
        _sectionProximityListener.Register();
    }

    public void NextLevel()
    {
        if (OnLeaveLevel)
        {
            OnLeaveLevel.Fire(this);
        }
        if (currentAmount >= numLevels)
        {
            if (EndDungeonTrigger!=null)
            {
                EndDungeonTrigger.Fire(this);
            }
            return;
        }
        Destroy(currentLevel.gameObject);
        Level nextLevel = PickNextLevel();
        InstanceLevel(nextLevel, true);
        currentAmount++;
        if (OnEnterLevel)
        {
            OnEnterLevel.Fire(this);
        }
    }

    protected void InstanceLevel(Level nextLevel, bool setCurrent = false)
    {
        GameObject level = new GameObject
        {
            name = nextLevel.name
        };
        level.transform.localPosition = Vector3.zero;
        level.transform.parent = transform;
        LevelInstance levelInstance = level.AddComponent(typeof(LevelInstance)) as LevelInstance;
        levelInstance.reference = nextLevel;
        if (setCurrent)
        {
            currentLevel = levelInstance;
        }
        else
        {
            level.SetActive(false);
        }
        GameObject nextSection;
        int currentAmount = 0;
        int numSections = Random.Range(nextLevel.MinSectionAmount, nextLevel.MaxSectionAmount + 1);
        nextSection = nextLevel.StartingSections[Random.Range(0, nextLevel.StartingSections.Count)];
        LevelSection currentSection = InstanceSection(levelInstance, nextSection, true);
        instantiatedSections.Add(currentSection);
        PlayerManager.Instance.GetPlayer(0).transform.position = currentSection.playerStart.transform.position;
        if (currentSection.OnArriveTrigger)
        {
            currentSection.OnArriveTrigger.Fire(this);
        }
        currentAmount++;

        LevelSectionConnector nextLevelSectionConnector;
        while (currentAmount < (numSections - 1)&&currentSection!=null)
        {
            nextLevelSectionConnector = currentSection.GetDisconnectedConnector();
            while (nextLevelSectionConnector != null && currentAmount < (numSections - 1))
            {
                nextSection = ConnectSection(nextLevelSectionConnector, nextLevel);
                InstanceSection(levelInstance, nextLevelSectionConnector, nextSection);
                nextLevelSectionConnector = currentSection.GetDisconnectedConnector();
                currentAmount++;
            }
            CheckAvailableConnections();
            currentSection = GetDisconnectedSection();
        }
    }

    private void CheckAvailableConnections()
    {
        foreach (LevelSection parentSection in instantiatedSections)
        {
            List<LevelSection> sections = parentSection.GetConnectedSections();
            foreach (LevelSection section in sections)
            {
                foreach(LevelSectionConnector connector in section.levelSectionConnectors)
                {
                    if (connector.ConnectedTo == null)
                    {
                        ConnectToExistent(connector);
                    }
                }
            }
        }
    }

    private void ConnectToExistent(LevelSectionConnector connectorParent)
    {
        foreach (LevelSection parentSection in instantiatedSections)
        {
            List<LevelSection> sections = parentSection.GetConnectedSections();
            foreach (LevelSection section in sections)
            {
                foreach (LevelSectionConnector connector in section.levelSectionConnectors)
                {
                    if (connector.ConnectedTo == null &&
                        !Object.ReferenceEquals(connectorParent, connector) &&
                        AreConnectable(connector.transform.position, connectorParent.transform.position)
                    )
                    {
                        connectorParent.ConnectedTo = connector;
                        connector.ConnectedTo = connectorParent;
                        return;
                    }
                }
            }
        }
    }

    private bool AreConnectable(Vector3 position,Vector3 position2)
    {
        float _distanceToNextPoint = (position - position2).magnitude;
        return _distanceToNextPoint < 1f;
    }

    private LevelSection GetDisconnectedSection()
    {
        foreach(LevelSection parentSection in instantiatedSections)
        {
            List<LevelSection> sections = parentSection.GetConnectedSections();
            foreach (LevelSection section in sections)
            {
                if (section.GetDisconnectedConnector() != null)
                {
                    return section;
                }
            }
        }
        return null;
    }

    protected GameObject ConnectSection(LevelSectionConnector levelSectionConnector, Level nextLevel)
    {
        List<int> triedSections;
        List<string> triedNames = new List<string>();
        GameObject nextSection;
        LevelSection tmp;
        string nextConnectorName;
        bool found = false;
        int newIndex;
        do
        {
            do
            {
                nextConnectorName = levelSectionConnector.GetNextConnectorName();
            } while (triedNames.Contains(nextConnectorName));
            triedNames.Add(nextConnectorName);

            triedSections = new List<int>();
            do
            {
                newIndex = Random.Range(0, nextLevel.levelSections.Count);
                if (DontRepeatSections && !triedSections.Contains(newIndex) && sectionsChosen.Contains(newIndex))
                {
                    triedSections.Add(newIndex);
                }
            } while (triedSections.Contains(newIndex));
            triedSections.Add(newIndex);
            nextSection = nextLevel.levelSections[newIndex];
            if (DontRepeatSections)
            {
                sectionsChosen.Add(newIndex);
            }
            while (!found && triedSections.Count < nextLevel.levelSections.Count)
            {
                tmp = nextSection.GetComponent<LevelSection>();
                foreach (LevelSectionConnector connector in tmp.levelSectionConnectors)
                {
                    if (connector!=null&&connector.name.Equals(nextConnectorName))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    do
                    {
                        newIndex = Random.Range(0, nextLevel.levelSections.Count);
                        if (DontRepeatSections && !triedSections.Contains(newIndex) && sectionsChosen.Contains(newIndex))
                        {
                            triedSections.Add(newIndex);
                        }
                    } while (triedSections.Contains(newIndex));
                    triedSections.Add(newIndex);
                    nextSection = nextLevel.levelSections[newIndex];
                    if (DontRepeatSections)
                    {
                        sectionsChosen.Add(newIndex);
                    }
                }
            }
        } while (!found && triedNames.Count < levelSectionConnector.Items.Count);
        return nextSection;
    }

    protected void InstanceSection(LevelInstance levelInstance, LevelSectionConnector connectorParent, GameObject nextSection)
    {
        GameObject section = Instantiate(nextSection, new Vector3(0, 0, 0), Quaternion.identity);
        section.transform.localPosition = Vector3.zero;
        LevelSection nextLevelSection = section.GetComponent<LevelSection>();
        instantiatedSections.Add(nextLevelSection);
        section.transform.parent = levelInstance.transform;
        section.SetActive(false);
        _ = section.AddComponent(typeof(LevelSectionInstance)) as LevelSectionInstance;
        foreach (LevelSectionConnector connector in nextLevelSection.levelSectionConnectors)
        {
            if (connector != null)
            {
                connector.levelSection = nextLevelSection;
                if (connector.name.Equals(connectorParent.lastConnected))
                {
                    connectorParent.ConnectedTo = connector;
                    connector.ConnectedTo = connectorParent;
                    section.transform.position = connectorParent.transform.position - connector.transform.position;
                }
            }
        }
    }

    protected LevelSection InstanceSection(LevelInstance levelInstance, GameObject nextSection, bool setCurrent = false)
    {
        GameObject section = Instantiate(nextSection, new Vector3(0, 0, 0), Quaternion.identity);
        section.transform.localPosition = Vector3.zero;
        LevelSection nextLevelSection = section.GetComponent<LevelSection>();
        section.transform.parent = levelInstance.transform;
        if (setCurrent)
        {
            levelInstance.currentSection = nextLevelSection;
        }
        else
        {
            section.SetActive(false);
        }
        foreach (LevelSectionConnector connector in nextLevelSection.levelSectionConnectors)
        {
            connector.levelSection = nextLevelSection;
        }
        return nextLevelSection;
    }

    protected Level PickNextLevel()
    {
        return dungeonData.levels[Random.Range(0, dungeonData.levels.Count)];
    }

    public override void Fire()
    {
        LevelSectionConnector currentConnector = currentLevel.currentSection.currentConnector;
        switch (StringParameter)
        {
            case "AwayNext":
                currentConnector.ConnectedTo.levelSection.gameObject.SetActive(false);
                break;
            case "NearNext":
                currentConnector.ConnectedTo.levelSection.gameObject.SetActive(true);
                break;
            case "Next":
                LevelSection levelSectionSource = EventSource.transform.parent.GetComponent<LevelSectionConnector>().levelSection;
                if (currentConnector.levelSection != levelSectionSource)
                {
                    currentLevel.currentSection = levelSectionSource;
                    if (levelSectionSource.OnArriveTrigger != null)
                    {
                        levelSectionSource.OnArriveTrigger.Fire(this);
                    }
                }
                break;
        }
    }

    public override void Cancel()
    {
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[3];
        callbacks[0] = new TriggerCallback("End Dungeon", EndDungeonTrigger,0);
        callbacks[1] = new TriggerCallback("On Enter Level", OnEnterLevel,1);
        callbacks[2] = new TriggerCallback("On Leave Level", OnLeaveLevel,2);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        switch (index)
        {
            case 0:
                EndDungeonTrigger = trigger;
                break;
            case 1:
                OnEnterLevel= trigger;
                break;
            case 2:
                OnLeaveLevel= trigger;
                break;
        }
    }
#endif

}
