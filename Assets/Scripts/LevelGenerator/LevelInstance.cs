﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInstance : MonoBehaviour
{
    public Level reference;
    public LevelSection currentSection;
}
