﻿using UnityEngine;

public class SetDataFromInventoryAmountTrigger : TriggerInterface
{
    public Inventory inventory;
    public string Item;
    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        int amount=realInventory.GetItemAmount(Item);
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        data.SetValue(amount.ToString());
                        break;
                    case DataHolder.Type.Number:
                        data.SetValue((float)amount);
                        break;
                    case DataHolder.Type.Integer:
                        data.SetValue(amount);
                        break;
                    case DataHolder.Type.Bool:
                        //not supported
                        break;
                }
                break;
            }
        }
    }

}
