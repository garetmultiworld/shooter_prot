﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddInventoriesTrigger : TriggerInterface
{

    public Inventory inventory;
    public InventoryItem[] items;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach(InventoryItem item in items)
        {
            realInventory.AddItem(item.Name, item.Amount);
        }
    }

}
