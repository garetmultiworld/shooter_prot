﻿using System.Collections.Generic;
using UnityEngine;

public class RemoveRandomFromInventoryTrigger : TriggerInterface
{

    public Inventory inventory;
    public int MinAmount;
    public int MaxAmount;
    public int MinTierSum;
    public int MaxTierSum;
    public RemoveRandomFromInventoryItem[] items;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        int CurrentAmount=0;
        int Amount;
        int CurrentTierSum = 0;
        int TierSum;
        int TierToRemove;
        int AmountToRemove;
        int Attempts = 0;
        int MaxAttempts=items.Length;
        Amount = Random.Range(MinAmount, MaxAmount + 1);
        TierSum= Random.Range(MinTierSum, MaxTierSum + 1);
        List<int> RemovableTiers=new List<int>();
        foreach(RemoveRandomFromInventoryItem item in items)
        {
            for(int iTier= Mathf.Max(1, item.MinTier); iTier<= item.MaxTier; iTier++)
            {
                if (!RemovableTiers.Contains(iTier))
                {
                    RemovableTiers.Add(iTier);
                }
            }
        }
        while(realInventory.HasItemsInTierRange(RemovableTiers) && CurrentAmount < Amount && CurrentTierSum < TierSum&& Attempts< MaxAttempts)
        {
            RemoveRandomFromInventoryItem item = items[Random.Range(0, items.Length)];
            if (Random.Range(0f, 100f)<item.chances)
            {
                TierToRemove = Random.Range(item.MinTier,item.MaxTier+1);
                AmountToRemove = Random.Range(item.MinAmount, item.MaxAmount + 1);
                realInventory.removeRandom(TierToRemove, AmountToRemove);
            }
            Attempts++;
        }
    }

}
