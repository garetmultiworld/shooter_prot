﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryData
{
    public string Name = "";
    public InventoryItemData[] items;
    private Inventory inventory;

    public void SetInventory(Inventory newInventory)
    {
        inventory = newInventory;
    }

    public Inventory GetInventory()
    {
        return inventory;
    }
}
