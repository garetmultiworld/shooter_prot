﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryHolder : MonoBehaviour
{

    public Inventory inventory;

    void Start()
    {
        if (inventory != null)
        {
            InventoryManager.Instance.getInventory(inventory);
        }
    }

}
