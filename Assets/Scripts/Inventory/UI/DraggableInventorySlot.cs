﻿using mw.player;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableInventorySlot : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{

    [SerializeField] private Canvas canvas;

    public static List<DraggableInventorySlot> Instances=new List<DraggableInventorySlot>();

    [HideInInspector]
    public static Player player=null;
    private RectTransform rect;
    private CanvasGroup canvasGroup;
    private Transform originalParent;

    public string Name = "";
    public int Amount;
    public bool Occupied=false;
    public GameObject ItemInstance=null;

    public int InitialInventoryIndex=-1;

    public TriggerInterface onBeginDrag;
    public TriggerInterface onEndDrag;
    public TriggerInterface onReceiveItem;
    public TriggerInterface onExchangeItem;
    public TriggerInterface onDropInAnotherSlot;
    public TriggerInterface onDropInEnvironment;

    private Vector2 OriginalPosition;

    private PersistanceListener persistanceListener;

    private void Awake()
    {
        Instances.Add(this);
        originalParent = this.transform.parent;
        rect = this.GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        if (InitialInventoryIndex > -1)
        {
            persistanceListener = gameObject.AddComponent(typeof(PersistanceListener)) as PersistanceListener;
            if (PersistanceManager.Instance.IsLoaded())
            {
                OnLoadedData();
            }
            else
            {
                CodeTrigger codeTrigger = gameObject.AddComponent(typeof(CodeTrigger)) as CodeTrigger;
                codeTrigger.handlerFire = OnLoadedData;
                persistanceListener.OnLoadData=codeTrigger;
            }
        }
    }

    public void OnLoadedData()
    {
        CheckPlayer();
        Inventory inventory = InventoryManager.Instance.getInventory(player.inventories[0]);
        if ((InitialInventoryIndex>-1&& inventory.items.Count <= InitialInventoryIndex)|| inventory.items[InitialInventoryIndex].Amount<1)
        {
            return;
        }
        SetItemInstance(Instantiate(
            inventory.GetItemUIPrefab(inventory.items[InitialInventoryIndex].Name)
        ));
        Occupied = true;
        Name = inventory.items[InitialInventoryIndex].Name;
        Amount = inventory.items[InitialInventoryIndex].Amount;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!Occupied)
        {
            eventData.pointerDrag = null;
            return;
        }
        if (onBeginDrag != null)
        {
            onBeginDrag.Fire();
        }
        OriginalPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y);
        this.transform.SetParent(canvas.transform);
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        rect.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        if (onEndDrag != null)
        {
            onEndDrag.Fire();
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit.transform != null)
        {
            DragPickable2D destination = hit.collider.GetComponent<DragPickable2D>();
            Debug.Log("DraggableInventorySlot drop: "+destination);
            if (destination != null)
            {
                Debug.Log(destination.Name + ", " + Name);
                Debug.Log(destination.CanReceive);
            }
            if (destination != null && destination.Name.Equals(Name) && destination.CanReceive)
            {
                CheckPlayer();
                ClearSlot();
                destination.Receive();
                if (onDropInEnvironment != null)
                {
                    onDropInEnvironment.Fire();
                }
            }
        }
        transform.SetParent(originalParent);
        rect.anchoredPosition = OriginalPosition;
    }

    public void ClearSlot()
    {
        CheckPlayer();
        InventoryManager.Instance.getInventory(player.inventories[0]).AddItem(Name, -Amount);
        Occupied = false;
        Name = "";
        Amount = 0;
        Destroy(ItemInstance);
        ItemInstance = null;
        PersistanceManager.Instance.SaveData();
    }

    public void OnDrop(PointerEventData eventData)
    {
        if(GameObject.ReferenceEquals(eventData.pointerDrag.gameObject, gameObject))
        {
            return;
        }
        if (eventData.pointerDrag != null)
        {
            DraggableInventorySlot source=eventData.pointerDrag.GetComponent<DraggableInventorySlot>();
            if (source != null)
            {
                if (!Occupied)
                {
                    Occupied = true;
                    Name = source.Name;
                    Amount = source.Amount;
                    SetItemInstance(source.ItemInstance);
                    source.Name = "";
                    source.Amount = 0;
                    source.Occupied = false;
                    source.ItemInstance = null;
                    if (onDropInAnotherSlot != null)
                    {
                        onDropInAnotherSlot.Fire();
                    }
                }
                else
                {
                    string tmpName = Name;
                    int tmpAmount = Amount;
                    GameObject tmpInstance = ItemInstance;
                    Name = source.Name;
                    Amount = source.Amount;
                    SetItemInstance(source.ItemInstance);
                    source.Name = tmpName;
                    source.Amount = tmpAmount;
                    source.SetItemInstance(tmpInstance);
                    if (onExchangeItem != null)
                    {
                        onExchangeItem.Fire();
                    }
                }
                return;
            }
        }
    }

    public void SetItemInstance(GameObject instance)
    {
        ItemInstance = instance;
        ItemInstance.transform.SetParent(transform, false);
        ItemInstance.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
    }

    public void CheckPlayer()
    {
        if (player == null)
        {
            player = (Player)FindObjectOfType(typeof(Player));
        }
    }

    public static bool CheckIfWithinBounds(Vector3 mousePosition,DragPickable2D pickable)
    {
        foreach(DraggableInventorySlot SlotInstance in Instances)
        {
            Vector2 localMousePosition = SlotInstance.rect.InverseTransformPoint(mousePosition);
            if (SlotInstance.rect.rect.Contains(localMousePosition) && !SlotInstance.Occupied)
            {
                SlotInstance.CheckPlayer();
                Inventory inventory = InventoryManager.Instance.getInventory(player.inventories[0]);
                inventory.AddItem(pickable.Name, pickable.Amount);
                SlotInstance.SetItemInstance(Instantiate(
                    inventory.GetItemUIPrefab(pickable.Name)
                ));
                SlotInstance.Occupied = true;
                SlotInstance.Name = pickable.Name;
                SlotInstance.Amount = pickable.Amount;
                PersistanceManager.Instance.SaveData();
                if (SlotInstance.onReceiveItem != null)
                {
                    SlotInstance.onReceiveItem.Fire();
                }
                return true;
            }
        }
        return false;
    }

}
