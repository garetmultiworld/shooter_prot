﻿using System;

public class EditorArrayUtils
{
    public static void RemoveAt<T>(ref T[] arr, int index)
    {
        for (int a = index; a < arr.Length - 1; a++)
        {
            arr[a] = arr[a + 1];
        }
        Array.Resize(ref arr, arr.Length - 1);
    }

    public static void InsertBefore<T>(ref T[] arr, int index)
    {
        Array.Resize(ref arr, arr.Length + 1);
        T newVal = arr[arr.Length - 1];
        for (int a = arr.Length - 1; a > index; a--)
        {
            arr[a] = arr[a-1];
        }
        arr[index] = newVal;
    }

    public static void InsertAfter<T>(ref T[] arr, int index)
    {
        Array.Resize(ref arr, arr.Length + 1);
        T newVal = arr[arr.Length - 1];
        for (int a = arr.Length - 1; a > (index+1); a--)
        {
            arr[a] = arr[a - 1];
        }
        arr[index+1] = newVal;
    }

    public static void MoveUp<T>(ref T[] arr, int index)
    {
        T temp = arr[index];
        arr[index] = arr[index - 1];
        arr[index - 1] = temp;
    }

    public static void MoveDown<T>(ref T[] arr, int index)
    {
        T temp = arr[index];
        arr[index] = arr[index + 1];
        arr[index + 1] = temp;
    }

}
