﻿public class SetTriggerFireableSequenceIndex : TriggerInterface
{
    public TriggerFireableSequence fireableSequence;
    public int Index;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        fireableSequence.SetIndex(Index);
    }

}
