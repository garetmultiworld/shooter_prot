﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ClickTrigger : MonoBehaviour
{

    public TriggerInterface Trigger;

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                return;
        }
        Trigger.Fire();
    }
}
