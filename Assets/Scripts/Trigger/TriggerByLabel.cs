﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerByLabel : TriggerInterface
{

    public GameObject Parent;
    public bool FromTarget;
    public string TriggerLabel;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        GameObject tempHolder;
        if (Parent != null)
        {
            tempHolder = Parent;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        TriggerInterface[] Triggers= tempHolder.GetComponents<TriggerInterface>();
        foreach (TriggerInterface trigger in Triggers)
        {
            if (TriggerLabel.Equals(trigger.GetLabel()))
            {
                trigger.Fire(this);
            }
        }
    }

}
