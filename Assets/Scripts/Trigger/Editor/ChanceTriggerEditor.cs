﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ChanceTrigger), true)]
public class ChanceTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(ChanceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ChanceTrigger");
    }

    protected void StorePrefabConflict(ChanceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        ChanceTrigger chanceTrigger = (ChanceTrigger)target;
        SetUpPrefabConflict(chanceTrigger);
        chanceTrigger.showChanceTrigger = EditorGUILayout.Foldout(
            chanceTrigger.showChanceTrigger,
            "Chance"
        );
        if (chanceTrigger.showChanceTrigger)
        {
            chanceTrigger.TriggerToFire = EditorUtils.SelectTrigger(this,"Trigger To Fire", chanceTrigger.TriggerToFire);
            chanceTrigger.Chances = EditorUtils.Slider(this,"Chances",chanceTrigger.Chances, 0f, 100f);
        }
        StorePrefabConflict(chanceTrigger);
    }

}
