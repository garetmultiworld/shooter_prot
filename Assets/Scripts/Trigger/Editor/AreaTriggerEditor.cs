﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AreaTrigger))]
public class AreaTriggerEditor : EditorBase
{
    protected void SetUpPrefabConflict(AreaTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AreaTrigger");
    }

    protected void StorePrefabConflict(AreaTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        AreaTrigger areaTrigger = (AreaTrigger)target;
        SetUpPrefabConflict(areaTrigger);
        bool hasColliders = false;
        Collider[] colliders = areaTrigger.GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            if (collider.isTrigger)
            {
                hasColliders = true;
            }
        }
        if (!hasColliders)
        {
            EditorGUILayout.HelpBox("This object must have at least one Collider with trigger enabled", MessageType.Warning);
            if (GUILayout.Button("Add CapsuleCollider"))
            {
                CapsuleCollider capsule = areaTrigger.gameObject.AddComponent(typeof(CapsuleCollider)) as CapsuleCollider;
                capsule.isTrigger = true;
                _hadChanges = true;
            }
            if (GUILayout.Button("Add BoxCollider"))
            {
                BoxCollider box = areaTrigger.gameObject.AddComponent(typeof(BoxCollider)) as BoxCollider;
                box.isTrigger = true;
                _hadChanges = true;
            }
            if (GUILayout.Button("Add SphereCollider"))
            {
                SphereCollider circle = areaTrigger.gameObject.AddComponent(typeof(SphereCollider)) as SphereCollider;
                circle.isTrigger = true;
                _hadChanges = true;
            }
        }
        areaTrigger.FilterLayer = EditorUtils.SelectLayerMask(this, "Detectable Layers", areaTrigger.FilterLayer);
        GUILayout.Label(" ");
        areaTrigger.OnEnter = EditorUtils.SelectTrigger(this, "On Enter", areaTrigger.OnEnter);
        areaTrigger.EnterAndExitExclusive = EditorUtils.Checkbox(this,
            "Enter And Exit Cancel Each Other?",
            areaTrigger.EnterAndExitExclusive
        );
        areaTrigger.OnExit = EditorUtils.SelectTrigger(this, "On Exit", areaTrigger.OnExit);

        EditorGUILayout.HelpBox("If you want to save the object that enters or exits this area you can save it on its respective target holder.", MessageType.Info);
        areaTrigger.TargetOnEnter = EditorUtils.TargetHolderField(this,
            "On Enter",
            areaTrigger.TargetOnEnter
        );
        areaTrigger.TargetOnExit = EditorUtils.TargetHolderField(this,
            "On Exit",
            areaTrigger.TargetOnExit
        );
        StorePrefabConflict(areaTrigger);
    }
}
