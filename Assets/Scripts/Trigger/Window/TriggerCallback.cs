﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCallback
{

    public string Name="";
    public int Index=0;
    public TriggerInterface Trigger=null;

    public TriggerCallback(string name, TriggerInterface trigger, int index)
    {
        Name = name;
        Trigger = trigger;
        Index=index;
    }

}
