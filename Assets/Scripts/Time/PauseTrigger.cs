﻿using UnityEngine;

public class PauseTrigger : TriggerInterface
{
    public override void Cancel()
    {
        Time.timeScale = 1;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Time.timeScale = 0;
    }

}
