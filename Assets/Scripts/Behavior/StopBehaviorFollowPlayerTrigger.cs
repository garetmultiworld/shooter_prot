﻿public class StopBehaviorFollowPlayerTrigger : TriggerInterface
{

    public BehaviorFollowPlayer behavior;
    public bool StopWithoutCallback=false;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        behavior.StopFollowing(StopWithoutCallback);
    }

}
