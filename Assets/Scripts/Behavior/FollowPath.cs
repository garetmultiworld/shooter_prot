﻿using System.Collections;
using UnityEngine;

public class FollowPath : MonoBehaviour
{

#if UNITY_EDITOR
	public bool showPath = false;
#endif

	public Path path;
	public bool AutoStart = false;
	public bool OneByOne = false;
	public float MovementSpeed = 1;
	public TriggerInterface OnStartMovement;
	public TriggerInterface OnStopMovement;

	private Rigidbody2D body;
	private bool isMoving;
	private Vector3 GoalPosition;
	private Vector3 Direction;
	private bool _waiting=false;
	private float _maxWaitTime = 0;
	protected bool _targetingIndex = false;
	protected int _targetIndex = 0;
	protected PathLocation _lastLocation = null;
	protected bool _destroyed=false;

	protected bool _wasMoving = false;

	protected bool _initialized = false;

	private Vector3 _newUp;

	void Awake()
	{
		if(path.Locations.Length == 0)
        {
			_destroyed = true;
			return;
		}
		body = GetComponent<Rigidbody2D>();
		isMoving = false;
        if (AutoStart)
        {
			BeginMovement();
		}
	}

	private void SpawnMarker(string label,Vector3 position)
    {
		GameObject objToSpawn = new GameObject(label+" " + GetInstanceID());
		objToSpawn.transform.SetParent(transform.parent);
		objToSpawn.transform.position = position;

	}

    public void Initialize()
    {
		_initialized = true;
		path.Initialize(transform.position);
	}

    public void BeginMovement()
    {
        if (path.Locations.Length <= 1)
        {
			return;
        }
        if (!_initialized)
        {
			Initialize();
		}
		GoToNextLocation();
		StartMovement();
	}

	protected void GoToNextLocation()
    {
		path.GoToNextLocation();
        if (path.EndReached())
        {
			StopMovement();
			return;
        }
		CalculateNewDirection();
	}

	private void CalculateNewDirection()
    {
		GoalPosition = path.GetCurrentPosition();
		_newUp = GoalPosition - transform.position;
		Direction = _newUp.normalized;
	}

	void OnDisable()
	{
        if (isMoving)
        {
			_wasMoving = true;
		}
		StopMovement();
	}

	void OnEnable()
	{
        if (_wasMoving)
        {
			_wasMoving = false;
			StartMovement();
		}
	}

	public void StopMovement()
    {
		isMoving = false;
		if (OnStopMovement)
		{
			OnStopMovement.Fire();
		}
	}

	public void StartMovement()
	{
		if (!_initialized)
		{
			Initialize();
		}
		isMoving = true;
		if (OnStartMovement)
		{
			OnStartMovement.Fire();
		}
		CalculateNewDirection();
		StartCoroutine(MoveAlongPath());
	}

	private IEnumerator MoveAlongPath()
    {
		while (isMoving&&!_destroyed)
        {
			while (_waiting && Time.time < _maxWaitTime)
			{
				_lastLocation = path.GetCurrentLocation();
				if (_lastLocation.OnLeaveTrigger != null)
				{
					_lastLocation.OnLeaveTrigger.Fire();
				}
				yield return null;
			}
			_waiting = false;
			transform.up = _newUp;
			body.MovePosition(new Vector2(
				(transform.position.x + Direction.x * MovementSpeed * Time.deltaTime),
				transform.position.y + Direction.y * MovementSpeed * Time.deltaTime)
			);
			if (path.Arrived(transform.position))
			{
				body.bodyType = RigidbodyType2D.Kinematic;
				transform.position = GoalPosition;
				body.bodyType = RigidbodyType2D.Dynamic;
				_lastLocation = path.GetCurrentLocation();
				if (_lastLocation.OnArriveTrigger != null)
				{
					_lastLocation.OnArriveTrigger.Fire();
				}
				if (_lastLocation.WaitTimeAfterArrival > 0)
				{
					_waiting = true;
					_maxWaitTime = Time.time + _lastLocation.WaitTimeAfterArrival;
				}
				if (OneByOne)
				{
					if (_targetingIndex)
					{
						if (_targetIndex == path.GetCurrentIndex())
						{
							_targetingIndex = false;
						}
						else
						{
							GoToNextLocation();
						}
					}
					if (!_targetingIndex)
					{
						StopMovement();
						//_changedPoint = true;
					}
				}
				else
				{
					GoToNextLocation();
				}
			}
			yield return null;
		}
	}

}
