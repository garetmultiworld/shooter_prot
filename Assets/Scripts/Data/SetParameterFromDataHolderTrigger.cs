﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetParameterFromDataHolderTrigger : TriggerInterface
{

    public DataHolder dataHolder;
    public TriggerInterface TriggerHolder;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TriggerHolder.StringParameter = dataHolder.Value;
    }


}
