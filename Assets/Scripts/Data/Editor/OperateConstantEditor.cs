﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(OperateConstant))]
public class OperateConstantEditor : DataHolderEditor
{
    protected void SetUpPrefabConflict(OperateConstant trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "OperateConstant");
    }

    protected void StorePrefabConflict(OperateConstant trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {

        OperateConstant operateConstant = (OperateConstant)target;

        base.OnInspectorGUI();

        SetUpPrefabConflict(operateConstant);

        operateConstant.showOperateConstant = EditorGUILayout.Foldout(
            operateConstant.showOperateConstant,
            "Operate"
        );
        if (operateConstant.showOperateConstant)
        {

            operateConstant.Holder = EditorUtils.GameObjectField(this, "Holder", operateConstant.Holder);

            operateConstant.LabelData = EditorUtils.TextField(this, "Data Name", operateConstant.LabelData);

            operateConstant.DataFromTarget = EditorUtils.Checkbox(this, "Is In Target Holder", operateConstant.DataFromTarget);

            operateConstant.DataOnLeft = EditorUtils.Checkbox(this, "Is Left Operand", operateConstant.DataOnLeft);

            operateConstant.operation = (OperateBase.Operation)EditorUtils.EnumPopup(this, operateConstant.operation);

            operateConstant.ConstantType = operateConstant.type;

            switch (operateConstant.ConstantType)
            {
                case DataHolder.Type.Number:
                    operateConstant.NumberValue = EditorUtils.FloatField(this, "Constant Value", operateConstant.NumberValue);
                    break;
                case DataHolder.Type.Integer:
                    operateConstant.IntegerValue = EditorUtils.IntField(this, "Constant Value", operateConstant.IntegerValue);
                    break;
            }
        }

        StorePrefabConflict(operateConstant);
    }

}
