﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceBetween : DataHolder
{

    public GameObject A;
    public bool GetTargetA;
    public GameObject B;
    public bool GetTargetB;

    public override float FloatValue
    {
        get {
            Vector3 posA;
            Vector3 posB;
            if (GetTargetA)
            {
                posA = A.GetComponent<TargetHolder>().Target.transform.position;
            }
            else
            {
                posA = A.transform.position;
            }
            if (GetTargetB)
            {
                posB = B.GetComponent<TargetHolder>().Target.transform.position;
            }
            else
            {
                posB = B.transform.position;
            }
            return Vector3.Distance(posA, posB);
        }
    }

}
