﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateTextFromDataTrigger : TriggerInterface
{

    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public Text text;
    public string Prefix;
    public string Suffix;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        text.text = Prefix+data.Value+ Suffix;
                        break;
                    case DataHolder.Type.Number:
                        text.text = Prefix + data.FloatValue.ToString() + Suffix;
                        break;
                    case DataHolder.Type.Integer:
                        text.text = Prefix + data.IntValue.ToString() + Suffix;
                        break;
                    case DataHolder.Type.Bool:
                        text.text = Prefix + data.BoolValue.ToString() + Suffix;
                        break;
                }
                break;
            }
        }
    }

}
