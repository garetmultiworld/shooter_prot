﻿using UnityEngine;

public class DataHolder : MonoBehaviour
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showDataHolder=true;
#endif
    public enum Type
    {
        Text,
        Number,
        Integer,
        Bool
    }

    public string Name;
    public Type type;

    public TriggerInterface OnChangeTrigger;

    public bool DebugData=false;

    [SerializeField]
    protected string InternalValue;
    [SerializeField]
    protected float InternalFloatValue;
    [SerializeField]
    protected int InternalIntValue;
    [SerializeField]
    protected bool InternalBoolValue;

    public virtual string Value
    {
        get { 
            return InternalValue; 
        }
    }

    public virtual float FloatValue
    {
        get
        {
            return InternalFloatValue;
        }
    }

    public virtual int IntValue
    {
        get
        {
            return InternalIntValue;
        }
    }

    public virtual bool BoolValue
    {
        get
        {
            return InternalBoolValue;
        }
    }

    public void Clear()
    {
        SetValue("");
        SetValue(0);
        SetValue(0f);
        SetValue(false);
    }

    public void SetValue(int newValue)
    {
        if (DebugData)
        {
            Debug.Log("Changing " + Name + " int value from: " + InternalIntValue + " To: " + newValue);
        }
        InternalIntValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

    public void SetValue(float newValue)
    {
        if (DebugData)
        {
            Debug.Log("Changing " + Name + " float value from: " + InternalFloatValue + " To: " + newValue);
        }
        InternalFloatValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

    public void SetValue(string newValue)
    {
        if (DebugData)
        {
            Debug.Log("Changing " + Name + " string value from: " + InternalValue + " To: " + newValue);
        }
        InternalValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

    public void SetValue(bool newValue)
    {
        if (DebugData)
        {
            Debug.Log("Changing " + Name + " bool value from: " + InternalBoolValue + " To: " + newValue);
        }
        InternalBoolValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

}
