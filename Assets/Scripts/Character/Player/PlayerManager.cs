﻿using System.Collections.Generic;
using UnityEngine;

namespace mw.player
{
    public class PlayerManager : MonoBehaviour
    {
        #region Static Instance
        private static PlayerManager instance;
        public static PlayerManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<PlayerManager>();
                    if (instance == null)
                    {
                        instance = new GameObject("Spawned InventoryManager", typeof(PlayerManager)).GetComponent<PlayerManager>();
                    }
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }
        #endregion

        private List<Player> players=new List<Player>();

        public void RegisterPlayer(Player player, int index)
        {
            if (index > 0)
            {
                players.Insert(index-1, player);
            }
            else
            {
                players.Add(player);
            }
            player.SetIndex(players.Count - 1);
        }

        public void ProcessPlayerEvent(int index, Player.Events evento)
        {
            if (index >= 0 && index < players.Count)
            {
                players[index].ProcessEvent(evento);
            }
        }

        public void ProcessPlayerAction(int index, Player.Actions Action)
        {
            if (index >= 0 && index < players.Count)
            {
                players[index].ProcessAction(Action);
            }
        }

        public Player GetPlayer(int index)
        {
            return players[index];
        }

    }
}