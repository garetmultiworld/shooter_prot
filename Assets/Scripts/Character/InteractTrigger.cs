﻿using mw.player;
using UnityEngine;

public class InteractTrigger : TriggerInterface
{

    public Character character;
    public float InteractRadius=1f;
    public TriggerInterface OnInteract;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Collider[] hitColliders = Physics.OverlapSphere(character.GetGameObject().transform.position, InteractRadius);
        Interactable interactable;
        foreach (Collider hitCollider in hitColliders)
        {
            interactable = hitCollider.gameObject.GetComponent<Interactable>();
            if (interactable != null)
            {
                interactable.Interact(character);
                if (OnInteract != null)
                {
                    OnInteract.Fire(this);
                }
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("On Interact", OnInteract,0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        OnInteract = trigger;
    }
#endif

}
