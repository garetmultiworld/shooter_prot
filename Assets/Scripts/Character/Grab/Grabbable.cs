﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{

    public TriggerInterface OnGrab;
    public TriggerInterface OnRelease;

    [HideInInspector]
    public GrabHandle handle;

}
