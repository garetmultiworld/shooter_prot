﻿using mw.player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigateOnClickState : CharacterState
{

    public float Speed = 1f;
    public GameObject Art = null;
    public bool FlipXArt = true;
    public bool ForceXArt = false;
    public float ValueForceXArt = 1;

    private Vector2[] Path =null;
    private int NavIndex = 0;
    private Vector2 NextPoint;
    private float NextTimer=0;

    private float StartingXScale;

    private void Start()
    {
        if (Art != null)
        {
            StartingXScale = Art.transform.localScale.x;
        }
    }

    public override bool CanBeActivated(Character _character)
    {
        return _character.CanMove();
    }

    public override void OnEnterState()
    {
        RefreshNavigation();
    }

    public override void OnExitState()
    {
        Path = null;
    }

    public override void OnFixedUpdate()
    {
        
    }

    public override void OnPauseState()
    {
        
    }

    public override void OnResumeState()
    {
        
    }

    public override void OnUpdate()
    {
        if (Path == null || NavIndex >= Path.Length || !character.CanMove())
        {
            /*if (Path == null) 
            {
                Debug.Log("The path dissapeared.");
            }else if(NavIndex >= Path.Length)
            {
                Debug.Log("Destination reached.");
            }else if (!character.CanMove())
            {
                Debug.Log("Character can't move, cancelling...");
            }*/
            character.EndState();
            return;
        }
        NextTimer -= Time.deltaTime;
        if (NextTimer<0)
        {
            NavIndex++;
            if(NavIndex >= Path.Length)
            {
                //Debug.Log("Destination reached.");
                character.EndState();
                return;
            }
            NextPoint = Path[NavIndex];
            NextTimer = Vector3.Distance(character.GetGameObject().transform.position, NextPoint) / Speed;
        }
    }

    public override void OnLateUpdate()
    {
        if (Art != null)
        {
            if (FlipXArt)
            {
                if (NextPoint.x < character.GetGameObject().transform.position.x)
                {
                    Art.transform.localScale = new Vector3(-StartingXScale, Art.transform.localScale.y, Art.transform.localScale.z);
                }
                else if (NextPoint.x > character.GetGameObject().transform.position.x)
                {
                    Art.transform.localScale = new Vector3(StartingXScale, Art.transform.localScale.y, Art.transform.localScale.z);
                }
            }
            else if (ForceXArt)
            {
                Art.transform.localScale = new Vector3(ValueForceXArt, Art.transform.localScale.y, Art.transform.localScale.z);
            }
        }
        character.GetGameObject().transform.position = Vector3.MoveTowards(character.GetGameObject().transform.position, NextPoint, Speed * Time.deltaTime);
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
        switch (stimuli)
        {
            case "NavigateOnClick":
                RefreshNavigation();
                break;
        }
    }

    public void ActivateArtXFlip()
    {
        FlipXArt = true;
        ForceXArt = false;
    }

    public void ForceArtXFlip(float xScale)
    {
        FlipXArt = false;
        ForceXArt = true;
        ValueForceXArt = xScale;
    }

    private void RefreshNavigation()
    {
        Path = TriggerSource.EventSource.GetComponent<NavigateOnClick>().Last_path;
        NavIndex = -1;
        NextTimer = 0;
    }
}
