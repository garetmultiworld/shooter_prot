﻿using mw.player;
using UnityEngine;

public class IdleState : CharacterState
{
    public override void OnEnterState()
    {
    }

    public override void OnExitState()
    {
    }

    public override void OnFixedUpdate()
    {
    }

    public override void OnPauseState()
    {
    }

    public override void OnResumeState()
    {
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
    }

    public override void OnUpdate()
    {
    }

    public override void OnLateUpdate()
    {

    }
}
