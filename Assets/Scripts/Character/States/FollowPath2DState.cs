﻿using mw.player;
using UnityEngine;

public class FollowPath2DState : CharacterState
{
#if UNITY_EDITOR
	public bool showPath = false;
#endif

	public Path path;
	public bool OneByOne = false;
	public float MovementSpeed = 1;

	private Rigidbody2D body;
	private bool isMoving;
	private Vector3 GoalPosition;
	private Vector3 Direction;
	private bool _waiting = false;
	private float _maxWaitTime = 0;
	protected bool _targetingIndex = false;
	protected int _targetIndex = 0;
	protected PathLocation _lastLocation = null;
	protected bool _destroyed = false;

	private Vector3 _initialPosition;

	public override void OnEnterState()
	{
		if (path.Locations.Length == 0)
		{
			_destroyed = true;
			return;
		}
		if (body == null)
		{
			body = character.GetComponent<Rigidbody2D>();
		}
		isMoving = false;
        if (_initialPosition == null)
        {
			_initialPosition = transform.position;
			path.Initialize(transform.position);
		}
		GoToNextLocation();
		StartMovement();
	}

	protected void GoToNextLocation()
	{
		path.GoToNextLocation();
		if (path.EndReached())
		{
			StopMovement();
			return;
		}
		GoalPosition = path.GetCurrentPosition();
		transform.up = GoalPosition - _initialPosition;
		Direction = transform.up.normalized;
	}

	public void StopMovement()
	{
		isMoving = false;
		character.EndState();
	}

	public void StartMovement()
	{
		isMoving = true;
	}

	public override void OnUpdate()
    {
		if (isMoving && !_destroyed)
		{
			while (_waiting && Time.time < _maxWaitTime)
			{
				_lastLocation = path.GetCurrentLocation();
				if (_lastLocation.OnLeaveTrigger != null)
				{
					_lastLocation.OnLeaveTrigger.Fire();
				}
			}
			_waiting = false;
			body.MovePosition(new Vector2(
				(transform.position.x + Direction.x * MovementSpeed * Time.deltaTime),
				transform.position.y + Direction.y * MovementSpeed * Time.deltaTime)
			);
			if (path.Arrived(transform.position))
			{
				body.bodyType = RigidbodyType2D.Kinematic;
				transform.position = GoalPosition;
				body.bodyType = RigidbodyType2D.Dynamic;
				_lastLocation = path.GetCurrentLocation();
				if (_lastLocation.OnArriveTrigger != null)
				{
					_lastLocation.OnArriveTrigger.Fire();
				}
				if (_lastLocation.WaitTimeAfterArrival > 0)
				{
					_waiting = true;
					_maxWaitTime = Time.time + _lastLocation.WaitTimeAfterArrival;
				}
				if (OneByOne)
				{
					if (_targetingIndex)
					{
						if (_targetIndex == path.GetCurrentIndex())
						{
							_targetingIndex = false;
						}
						else
						{
							GoToNextLocation();
						}
					}
					if (!_targetingIndex)
					{
						StopMovement();
					}
				}
				else
				{
					GoToNextLocation();
				}
			}
		}
	}

	public override void OnLateUpdate()
	{

	}

	public override void OnPauseState()
    {
    }

    public override void OnResumeState()
    {
    }

    public override void OnFixedUpdate()
    {
    }

    public override void OnExitState()
    {
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
    }
}
