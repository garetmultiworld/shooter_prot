﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigateState : CharacterState
{

    public float Speed = 1f;
    public GameObject Art = null;

    private Vector2[] Path = null;
    private int NavIndex = 0;
    private Vector2 NextPoint;
    private float NextTimer = 0;

    private float StartingXScale;

    private NavigateTrigger lastTrigger;

    private void Start()
    {
        if (Art != null)
        {
            StartingXScale = Art.transform.localScale.x;
        }
    }

    public override bool CanBeActivated(Character _character)
    {
        return _character.CanMove();
    }

    public override void OnEnterState()
    {
        lastTrigger = TriggerSource.EventSource.GetComponent<NavigateTrigger>();
        ResetPath();
    }

    protected void ResetPath()
    {
        Path = lastTrigger.Last_path;
        NavIndex = -1;
        NextTimer = 0;
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
    }

    public override void OnExitState()
    {
        if(lastTrigger!=null&& lastTrigger.OnNavigationEnd != null)
        {
            lastTrigger.OnNavigationEnd.Fire();
        }
        lastTrigger = null;
        Path = null;
    }

    public override void OnFixedUpdate()
    {
        
    }

    public override void OnPauseState()
    {
        
    }

    public override void OnResumeState()
    {
        
    }

    public override void OnUpdate()
    {
        if (Path == null || NavIndex >= Path.Length || !character.CanMove())
        {
            if (lastTrigger != null && lastTrigger.OnArrive != null)
            {
                lastTrigger.OnArrive.Fire();
            }
            if (lastTrigger.HasAnotherDestination())
            {
                lastTrigger.GoToDestination();
                ResetPath();
                if (lastTrigger.OnNextDestination != null)
                {
                    lastTrigger.OnNextDestination.Fire();
                }
            }
            else
            {
                character.EndState();
            }
            return;
        }
        NextTimer -= Time.deltaTime;
        if (NextTimer < 0)
        {
            NavIndex++;
            if (NavIndex >= Path.Length)
            {
                character.EndState();
                return;
            }
            NextPoint = Path[NavIndex];
            NextTimer = Vector3.Distance(character.GetGameObject().transform.position, NextPoint) / Speed;
        }
        if (Art != null)
        {
            if (NextPoint.x < character.GetGameObject().transform.position.x)
            {
                Art.transform.localScale = new Vector3(-StartingXScale, Art.transform.localScale.y, Art.transform.localScale.z);
            }
            else if (NextPoint.x > character.GetGameObject().transform.position.x)
            {
                Art.transform.localScale = new Vector3(StartingXScale, Art.transform.localScale.y, Art.transform.localScale.z);
            }
        }
        character.GetGameObject().transform.position = Vector3.MoveTowards(character.GetGameObject().transform.position, NextPoint, Speed * Time.deltaTime);
    }
    public override void OnLateUpdate()
    {

    }
}
