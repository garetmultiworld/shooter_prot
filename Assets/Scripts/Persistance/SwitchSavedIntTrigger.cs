﻿public class SwitchSavedIntTrigger : TriggerInterface
{

    public string dataName;
    public SwitchSavedIntCaseTrigger[] Cases;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        int value = PersistanceManager.Instance.GetIntData(dataName);
        foreach (SwitchSavedIntCaseTrigger theCase in Cases)
        {
            if (theCase != null &&
                theCase.IntValue==value &&
                theCase.Trigger != null)
            {
                theCase.Trigger.Fire(this);
            }
        }
    }

}
