﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadDataTrigger : TriggerInterface
{

    public bool switchScene = false;
    public TriggerInterface OnExistingFile;
    public TriggerInterface OnUnexistingFile;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        PersistanceManager.Instance.OnExistingFile = OnExistingFile;
        PersistanceManager.Instance.OnUnexistingFile = OnUnexistingFile;
        PersistanceManager.Instance.LoadData(switchScene);
    }

}
