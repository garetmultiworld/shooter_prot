﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerIfPersistanceExists : TriggerInterface
{

    public TriggerInterface IfExists;
    public TriggerInterface IfNotExists;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (PersistanceManager.Instance.FileExists())
        {
            if (IfExists != null)
            {
                IfExists.Fire();
            }
        }
        else
        {
            if (IfNotExists != null)
            {
                IfNotExists.Fire();
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0]= new TriggerCallback("If Exists", IfExists, 0);
        callbacks[1] = new TriggerCallback("If Not Exists", IfNotExists, 1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            IfExists = trigger;
        }
        else
        {
            IfNotExists = trigger;
        }
    }
#endif

}
