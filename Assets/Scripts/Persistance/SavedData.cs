﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class SavedData
{
    public SavedString[] Strings=new SavedString[0];
    public SavedInt[] Ints = new SavedInt[0];
    public SavedFloat[] Floats = new SavedFloat[0];
    public SavedBool[] Bools = new SavedBool[0];
    public string SceneToLoad="";
    public string NextStartingPoint = "";
    [HideInInspector]
    public I18nLanguages CurrentLanguage;
    [HideInInspector]
    public InventoryData[] inventories;
    [HideInInspector]
    public string hash;
    [HideInInspector]
    public Dictionary<string, int> StringDict=new Dictionary<string, int>();
    public Dictionary<string, int> IntDict = new Dictionary<string, int>();
    public Dictionary<string, int> FloatDict = new Dictionary<string, int>();
    public Dictionary<string, int> BoolDict = new Dictionary<string, int>();

    public void ClearData()
    {
        Strings = new SavedString[0];
        Ints = new SavedInt[0];
        Floats = new SavedFloat[0];
        Bools = new SavedBool[0];
        StringDict = new Dictionary<string, int>();
        IntDict = new Dictionary<string, int>();
        FloatDict = new Dictionary<string, int>();
        BoolDict = new Dictionary<string, int>();
    }

    public void InitDict()
    {
        BoolDict.Clear();
        for (int i = 0; i < Bools.Length; i++)
        {
            BoolDict.Add(Bools[i].Name, i);
        }
        IntDict.Clear();
        for (int i = 0; i < Ints.Length; i++)
        {
            IntDict.Add(Ints[i].Name, i);
        }
        StringDict.Clear();
        for (int i = 0; i < Strings.Length; i++)
        {
            StringDict.Add(Strings[i].Name, i);
        }
        FloatDict.Clear();
        for (int i = 0; i < Floats.Length; i++)
        {
            FloatDict.Add(Floats[i].Name, i);
        }
    }

    public bool GetBool(string DataName)
    {
        if (!BoolDict.ContainsKey(DataName))
        {
            return false;
        }
        return Bools[BoolDict[DataName]].Value;
    }

    public void SetBool(string DataName, bool Value)
    {
        if (!BoolDict.ContainsKey(DataName))
        {
            AddBool(DataName,Value);
        }
        else
        {
            Bools[BoolDict[DataName]].Value = Value;
        }
    }

    private void AddBool(string DataName, bool Value)
    {
        Array.Resize(ref Bools, Bools.Length + 1);
        Bools[Bools.Length - 1] = new SavedBool
        {
            Name = DataName,
            Value = Value
        };
        BoolDict.Add(DataName, Bools.Length - 1);
    }

    public int GetInt(string DataName)
    {
        if (!IntDict.ContainsKey(DataName))
        {
            return 0;
        }
        return Ints[IntDict[DataName]].Value;
    }

    public void SetInt(string DataName, int Value)
    {
        if (!IntDict.ContainsKey(DataName))
        {
            AddInt(DataName, Value);
        }
        else
        {
            Ints[IntDict[DataName]].Value = Value;
        }
    }

    private void AddInt(string DataName, int Value)
    {
        Array.Resize(ref Ints, Ints.Length + 1);
        Ints[Ints.Length - 1] = new SavedInt
        {
            Name = DataName,
            Value = Value
        };
        IntDict.Add(DataName, Ints.Length - 1);
    }

    public float GetFloat(string DataName)
    {
        if (!FloatDict.ContainsKey(DataName))
        {
            return 0;
        }
        return Floats[FloatDict[DataName]].Value;
    }

    public void SetFloat(string DataName, float Value)
    {
        if (!FloatDict.ContainsKey(DataName))
        {
            AddFloat(DataName, Value);
        }
        else
        {
            Floats[FloatDict[DataName]].Value = Value;
        }
    }

    private void AddFloat(string DataName, float Value)
    {
        Array.Resize(ref Floats, Floats.Length + 1);
        Floats[Floats.Length - 1] = new SavedFloat
        {
            Name = DataName,
            Value = Value
        };
        FloatDict.Add(DataName, Floats.Length - 1);
    }

    public string GetString(string DataName)
    {
        if (!StringDict.ContainsKey(DataName))
        {
            return "";
        }
        return Strings[StringDict[DataName]].Value;
    }

    public void SetString(string DataName, string Value)
    {
        if (!StringDict.ContainsKey(DataName))
        {
            AddString(DataName, Value);
        }
        else
        {
            Strings[StringDict[DataName]].Value = Value;
        }
    }

    private void AddString(string DataName, string Value)
    {
        Array.Resize(ref Strings, Strings.Length + 1);
        Strings[Strings.Length - 1] = new SavedString
        {
            Name = DataName,
            Value = Value
        };
        StringDict.Add(DataName, Strings.Length - 1);
    }

    public string GetJson(bool prettyPrint = false)
    {
        return JsonUtility.ToJson(this, prettyPrint);
    }

    public void SetJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
    }
}
