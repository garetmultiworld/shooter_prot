﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSavedIntCaseTrigger : MonoBehaviour
{
    public int IntValue;
    public TriggerInterface Trigger;

    public bool DataCoincides(int data)
    {
        return (IntValue == data);
    }

}
