﻿using System.Collections.Generic;
using UnityEngine;

namespace mw.Pathfinding
{
    public class AINavMeshGenerator : MonoBehaviour
    {

        enum Directions { Right, DownRight, Down, DownLeft, Left, UpLeft, Up, UpRight }

        /*[SerializeField]
        private float updateInterval = 0.1f;*/
        [SerializeField]
        private float pointDistributionSize = 0.5f;
        [SerializeField]
        LayerMask destroyNodeMask;
        [SerializeField]
        LayerMask obstacleMask;

        public Rect size;
        private float _calculated_x_min;
        private float _calculated_y_min;
        private float _calculated_x_max;
        private float _calculated_y_max;

        //private float updateTimer = 0;
        private List<Node> grid = null;
        private List<Node> Grid
        {
            get
            {
                if (grid == null)
                {
                    GenerateNewGrid();
                }
                return grid;
            }
        }
        private Dictionary<Vector2, Node> positionNodeDictionary = new Dictionary<Vector2, Node>();
        public Pathfinder pathfinder = null;

        public void GenerateNewGrid()
        {
            FillOutGrid();
            DestroyBadNodes();
            CheckForBadNodes();
        }


        public LayerMask GetAvoidanceMasks()
        {
            return destroyNodeMask | obstacleMask;
        }

        private void Awake()
        {
            pathfinder = new Pathfinder(this);
        }

        private void Start()
        {
            GenerateNewGrid();
            _calculated_x_min = -(size.width / 2) + size.xMin;
            _calculated_y_min = -(size.height / 2) + size.yMin;
            _calculated_x_max = (size.width / 2) + size.xMin;
            _calculated_y_max = (size.height / 2) + size.yMin;
            //updateTimer = updateInterval;
        }

        public void Refresh()
        {
            GenerateNewGrid();
            CheckForBadNodes();
        }

        private void FillOutGrid()
        {
            grid = new List<Node>();
            positionNodeDictionary.Clear();
            Vector2 currentPoint = new Vector2((size.x - size.width / 2) + pointDistributionSize, (size.y + size.height / 2) - pointDistributionSize);
            int iteration = 0;
            bool alternate = false;
            bool cacheIteration = false;
            int length = -1;
            int yLength = 0;
            while (true)
            {
                iteration++;
                Node newNode = new Node(currentPoint);
                Grid.Add(newNode);
                positionNodeDictionary.Add(currentPoint, newNode);
                currentPoint += new Vector2(pointDistributionSize * 2, 0);
                if (currentPoint.x > size.x + size.width / 2)
                {
                    if (length != -1)
                    {
                        while (iteration < length)
                        {
                            Node extraNode = new Node(currentPoint);
                            Grid.Add(extraNode);
                            iteration++;
                        }
                    }
                    else
                    {
                        Node extraNode = new Node(currentPoint);
                        Grid.Add(extraNode);
                    }
                    currentPoint = new Vector2((size.x - size.width / 2) + (alternate ? pointDistributionSize : 0), currentPoint.y - pointDistributionSize);
                    alternate = !alternate;
                    cacheIteration = true;
                    yLength++;
                }
                if (currentPoint.y < size.y - size.height / 2)
                {
                    break;
                }
                if (cacheIteration)
                {
                    if (length == -1)
                    {
                        length = iteration + 1;
                    }
                    iteration = 0;
                    cacheIteration = false;
                }
            }
            for (int i = 0; i < Grid.Count; i++)
            {
                for (int direction = 0; direction < Grid[i].connections.Length; direction++)
                {
                    Grid[i].connections[direction] = GetNodeFromDirection(i, (Directions)direction, length);
                }
            }
        }

        private void DestroyBadNodes()
        {
            //First check if each node is inside a destroy mask
            for (int i = Grid.Count - 1; i >= 0; i--)
            {
                Collider2D hit = Physics2D.OverlapCircle(Grid[i].position, 0.01f, destroyNodeMask);
                if (hit != null)
                {
                    //At this point, we know this node is bad, and we must destroy it. For humanity itself.
                    for (int j = 0; j < Grid[i].connections.Length; j++)
                    {
                        //Go through all the connections to this node
                        if (Grid[i].connections[j] != null)
                        {
                            for (int k = 0; k < Grid[i].connections[j].connections.Length; k++)
                            {
                                //Set the nodes connections reference to this node to null, because it no longer exists.
                                //Is that confusing? It sounds confusing.
                                if (Grid[i].connections[j].connections[k] != null)
                                {
                                    if (Grid[i].connections[j].connections[k] == Grid[i])
                                    {
                                        Grid[i].connections[j].connections[k] = null;
                                    }
                                }
                            }
                        }
                    }
                    Grid.RemoveAt(i);
                }
            }
        }

        public void CheckForBadNodes()
        {
            for (int i = 0; i < Grid.Count; i++)
            {
                Grid[i].Reset();
            }

            //Make any node with a destroyed outside have an extra layer barrier around it, so that they dont get too close to walls
            for (int i = 0; i < Grid.Count; i++)
            {
                if (Grid[i].valid)
                {
                    for (int j = 0; j < Grid[i].connections.Length; j++)
                    {
                        Node connection = Grid[i].connections[j];
                        if (connection == null)
                        {
                            Grid[i].valid = false;
                        }
                    }
                }
            }

            //Then check if the node is inside a normal mask to disable it.
            for (int i = 0; i < Grid.Count; i++)
            {
                if (Grid[i].valid)
                {
                    Collider2D hit = Physics2D.OverlapCircle(Grid[i].position, 0.05f, obstacleMask);
                    if (hit != null)
                    {
                        Grid[i].valid = false;
                        Grid[i].associatedObject = hit.transform.gameObject;
                    }
                }
            }
        }

        Node GetNodeFromDirection(int nodeIndex, Directions direction, int length)
        {
            int index = -1;
            bool isStartOfRow = (nodeIndex + 1) % length == 1;
            bool isEndOfRow = (nodeIndex + 1) % length == 0;
            bool isOddRow = (((nodeIndex + 1) - Mathf.FloorToInt((nodeIndex) % length)) / length) % 2 == 0;

            switch (direction)
            {
                case Directions.Right:
                    if (isEndOfRow) return null;
                    index = nodeIndex + 1;
                    break;
                case Directions.DownRight:
                    if (isEndOfRow && isOddRow) return null;
                    index = nodeIndex + length + (isOddRow ? 1 : 0);
                    break;
                case Directions.Down:
                    index = nodeIndex + length * 2;
                    break;
                case Directions.DownLeft:
                    if (isStartOfRow && !isOddRow) return null;
                    index = nodeIndex + (length - (isOddRow ? 0 : 1));
                    break;
                case Directions.Left:
                    if (isStartOfRow) return null;
                    index = nodeIndex - 1;
                    break;
                case Directions.UpLeft:
                    if (isStartOfRow && !isOddRow) return null;
                    index = nodeIndex - (length + (isOddRow ? 0 : 1));
                    break;
                case Directions.Up:
                    index = nodeIndex - length * 2;
                    break;
                case Directions.UpRight:
                    if (isEndOfRow && isOddRow) return null;
                    index = nodeIndex - (length - (isOddRow ? 1 : 0));
                    break;
            }

            if (index >= 0 && index < Grid.Count)
            {
                return Grid[index];
            }
            else
            {
                return null;
            }
        }

        public Node FindClosestNode(Vector2 position, bool mustBeGood = false, GameObject associatedObject = null)
        {
            Node closest = null;
            float current = float.MaxValue;
            for (int i = 0; i < Grid.Count; i++)
            {
                if (!mustBeGood || Grid[i].valid || associatedObject == Grid[i].associatedObject)
                {
                    float distance = Vector2.Distance(Grid[i].position, position);
                    if (distance < current)
                    {
                        current = distance;
                        closest = Grid[i];
                    }
                }
            }

            return closest;
        }

        public void ClearGrid()
        {
            Grid.Clear();
        }

        public bool IsWithinBounds(Vector3 point)
        {
            return (
                point.x >= _calculated_x_min &&
                point.x <= _calculated_x_max &&
                point.y >= _calculated_y_min &&
                point.y <= _calculated_y_max
                );
        }

        /*void Update()
        {
            if (Grid == null)
            {
                GenerateNewGrid();
            }

            //We update the bad nodes constantly, so as objects or enemies move, the grid automatically adjusts itself.
            updateTimer -= Time.deltaTime;
            if (updateTimer <= 0)
            {
                updateTimer = updateInterval;
                CheckForBadNodes();
            }
        }*/

        void OnDrawGizmosSelected()
        {
            if (Grid == null)
            {
                return;
            }
            for (int i = 0; i < Grid.Count; i++)
            {
                for (int j = 0; j < Grid[i].connections.Length; j++)
                {
                    if (Grid[i].connections[j] != null)
                    {
                        Gizmos.color = Grid[i].valid && Grid[i].connections[j].valid ? Color.green : Color.red;
                        Gizmos.DrawLine(Grid[i].position, Grid[i].connections[j].position);
                    }
                }
            }
            for (int i = 0; i < Grid.Count; i++)
            {
                Gizmos.color = Grid[i].valid ? Color.blue : Color.red;
                Gizmos.DrawCube(Grid[i].position, Vector3.one * 0.2f);
            }
        }

    }
}