﻿using mw.Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshNavMeshTrigger : TriggerInterface
{
    public AINavMeshGenerator navMesh;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        navMesh.Refresh();
    }

}
