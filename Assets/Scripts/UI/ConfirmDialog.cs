﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmDialog : MonoBehaviour
{

    public string Name;
    public TriggerInterface OnShow;
    public TriggerInterface OnYes;
    public TriggerInterface OnNo;

    [HideInInspector]
    public TriggerInterface OnYesCallback;
    [HideInInspector]
    public TriggerInterface OnNoCallback;

    void Start()
    {
        ConfirmDialogManager.Instance.RegisterDialog(this);
        gameObject.SetActive(false);
    }

    public void Yes()
    {
        if (OnYesCallback != null)
        {
            OnYesCallback.Fire();
            OnYesCallback = null;
        }
        if (OnYes != null)
        {
            OnYes.Fire();
        }
        gameObject.SetActive(false);
    }

    public void No()
    {
        if (OnNoCallback != null)
        {
            OnNoCallback.Fire();
            OnNoCallback = null;
        }
        if (OnNo != null)
        {
            OnNo.Fire();
        }
        gameObject.SetActive(false);
    }

}
