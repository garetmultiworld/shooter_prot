﻿using UnityEngine;
using UnityEngine.UI;

public class SetTextTrigger : TriggerInterface
{
    public string NewText;
    public Text text;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        text.text = NewText;
    }

}
