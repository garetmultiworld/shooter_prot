﻿using Cinemachine;
using UnityEngine;

public class FollowTargetTrigger : TriggerInterface
{

    public CinemachineVirtualCamera cinemachineVirtualCamera;
    public GameObject Target;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineVirtualCamera.m_Follow = Target.transform;
    }

}
