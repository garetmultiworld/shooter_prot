﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStopInputTrigger : MonoBehaviour
{

    public string[] InputNames=new string[1];
    public TriggerInterface Trigger;
    public float Cooldown = 0.1f;

    protected bool Ready = true;
    protected float CooldownTimer = 0f;

    void Update()
    {
        if (!Ready)
        {
            CooldownTimer += Time.deltaTime;
            if (CooldownTimer >= Cooldown)
            {
                Ready = true;
            }
        }
        if (Ready)
        {
            bool AllInactive = true;
            foreach (string InputName in InputNames)
            {
                if (Input.GetAxis(InputName) != 0 && Trigger != null)
                {
                    AllInactive = false;
                }
            }
            if (AllInactive)
            {
                Trigger.Fire();
                CooldownTimer = 0f;
                Ready = false;
            }
        }
    }

    public void SetReady(bool ready)
    {
        enabled = ready;
        Ready = ready;
    }

}
